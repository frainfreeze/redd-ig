import bot
from apscheduler.schedulers.background import BackgroundScheduler
import os
import time
import subprocess


zz = bot.freq
def checkUpdate():
    return(False)

def progUpdate():
    isThereUpdate = checkUpdate()

    if isThereUpdate == True:
        print "update"
        subprocess.Popen(['sudo', 'git', 'pull', "https://frainfreeze@bitbucket.org/frainfreeze/redd-ig.git"])
        bot.main()
    else:
        print "no update, running bot"
        bot.main()


# lets execute job every given time
if __name__ == '__main__':
    progUpdate()
    scheduler = BackgroundScheduler()
    scheduler.add_job(progUpdate, 'interval', hours=zz)
    scheduler.start()
    print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))

    try:
        # This is here to simulate application activity (which keeps the main thread alive).
        while True:
            time.sleep(2)
    except (KeyboardInterrupt, SystemExit):
        # Not strictly necessary if daemonic mode is enabled but should be done if possible
        scheduler.shutdown()
else:
    print "imported bot script"