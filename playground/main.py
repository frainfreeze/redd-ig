import json
import os
import requests
import urllib3
urllib3.disable_warnings()
print "json test"

bannedSubs = ["askreddit", "showerthoughts"]
def getJSON():
    ''' downloads reddit json and returns it as a string'''
    # we could use urllib but then reddit servers block us every now and then for being bot
    #url = "http://www.reddit.com/r/popular/hot.json?count=20"
    #response = urllib.urlopen(url)
    #data = json.dumps(json.loads(response.read()))
    
    r = requests.get('http://www.reddit.com/r/popular/hot.json?count=20', headers = {'User-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'})
    theJSON = json.dumps(json.loads(r.text))
    return theJSON



data = getJSON()

script = open('testData.json', 'w')
script.truncate()
script.write(data)
script.close()
del data

with open('testData.json') as data_file:    
    data = json.load(data_file)

a = 0
while a <= 24:
    try:
        print "\n\niteration N." + str(a)

        subreddit = data["data"]["children"][a]["data"]["subreddit"]
        score = data["data"]["children"][a]["data"]["score"]
        over_18 = data["data"]["children"][a]["data"]["over_18"]
        post_hint = data["data"]["children"][a]["data"]["post_hint"]
        url = data["data"]["children"][a]["data"]["url"]
        title = data["data"]["children"][a]["data"]["title"]

        #print "subreddit " + subreddit
        #print "score " + str(score)
        #print "over 18 " + str(over_18)
        #print "post hint " + post_hint
        #print "url " + url
        #print "title " + title
        
        #check if banned
        if "subreddit" in bannedSubs:
            print "subreddit <" + str(subreddit) + "> is disallowed going for next one"
            pass
        else:

            #check if nsfw
            if over_18 == True:
                print "subreddit <" + str(subreddit) + "> is NSFW going for next one"
                pass
            else:
                #check if image
                if post_hint == "image":
                    print "we have image from subreddit " + str(subreddit)
                
                #or a link
                elif post_hint == "link":
                    print "we have link from subreddit <" + str(subreddit) + ">, lets bail"
                    pass

                # if nothing lets pass
                else:
                    print "no usable content in <" + str(subreddit) + ">"

    except:
        print "error on a" + str(a)
        pass
    
    a = a + 1

print "done!"
try:
        os.remove('testData.json')
except OSError, e:  ## if failed, report it back to the user ##
        print ("Error: %s - %s." % (e.filename,e.strerror))

        
#url = 'http://example.com/img.png'
#response = requests.get(url, stream=True)
#with open('img.png', 'wb') as out_file:
#    shutil.copyfileobj(response.raw, out_file)
#del response

''''

{
 "data":[
         {"id":"blabla","iscategorical":"0"},
         {"id":"blabla","iscategorical":"0"}
        ],
"masks":
         {"id":"valore"},
"om_points":"value",
"parameters":
         {"id":"valore"}
}
data["data"][0]["id"]  # will return 'blabla'
data["masks"]["id"]    # will return 'valore'
data["om_points"]      # will return 'value' '''