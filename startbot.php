<?php

ob_end_flush();
ini_set("output_buffering", "0");
ob_implicit_flush(true);
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

function echoEvent($datatext) {
    echo "data: ".implode("\ndata: ", explode("\n", $datatext))."\n\n";
}

echoEvent("Start!");



// Get the contents of the using file_get_contents
$file_content = file_get_contents('config.txt');

// convert the file to an array containing lines
$lines = explode("\r\n", $file_content);

// Extract the Values
$username = $lines[0]; // 1st line
echo $username;
$password = $lines[1]; // 2nd line
echo $password;


$proc = popen("python bot.py $username $password", 'r');
while (!feof($proc)) {
    echoEvent(fread($proc, 4096));
}
echoEvent("Finish!");