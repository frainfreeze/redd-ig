#!/bin/bash
# autoinstall script for bot reddit->instagram

sudo apt-get -y install php7.0 php7.0-curl php7.0-mbstring php7.0-gd ffmpeg python-pip imagemagick
sudo pip install requests
sudo pip install simplejson
sudo pip install apscheduler

sudo php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
sudo php composer-setup.php
sudo php -r "unlink('composer-setup.php');"

cd ~/Desktop

sudo git clone https://github.com/mgp25/Instagram-API.git
cd Instagram-API
sudo composer install

sudo rm -r examples
sudo git clone https://frainfreeze@bitbucket.org/frainfreeze/redd-ig.git


sudo chmod 777 redd-ig
cd redd-ig
sudo chmod 777 ./aspectcrop.sh

#sudo nano generate_hash.php
#sudo php generate_hash.php 

echo "ctrl-c when done with web interface"
sudo php -S localhost:8143