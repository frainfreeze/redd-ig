from datetime import datetime
import time
import os
from os.path import basename
import subprocess
import requests
import urllib, urllib3, json
from urllib2 import urlopen  
import logging
logging.basicConfig()
urllib3.disable_warnings()

##### config ###########
lines = [] #here we will store our config variables

f = open( "config.txt", "r" )
for line in f:
    lines.append(line)
f.close()

# values from config
user = lines[0].strip()
pwd = lines[1].strip()
freq = int(lines[2].strip())
subs = lines[3]
bannedSubs = subs.split()
#########################

supportedPictureFormats = ['.png']
supportedMovingPictureFormats = ['.gifv','.gif']

def getJSON():
    ''' downloads reddit json and returns it as a string'''
    r = requests.get('http://www.reddit.com/r/popular/hot.json?count=20', headers = {'User-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'})
    theJSON = json.dumps(json.loads(r.text))
    return theJSON

def getContent():
    ''' here happens most of the magic,
        returns title filename subreddit and extension'''
    #to do: remove content we have
    #to do: compare scores for highest score
    #what iteration is usable

    usableIteration = 26      
    data = getJSON()

    script = open('testData.json', 'w')
    script.truncate()
    script.write(data)
    script.close()
    del data

    with open('testData.json') as data_file:    
        data = json.load(data_file)
    
    #magic loop
    a = 0
    while a <= 24:
        try:
            print "\n\niteration N." + str(a)

            subreddit = data["data"]["children"][a]["data"]["subreddit"]
            over_18 = data["data"]["children"][a]["data"]["over_18"]
            post_hint = data["data"]["children"][a]["data"]["post_hint"]
            title = data["data"]["children"][a]["data"]["title"]
            url = data["data"]["children"][a]["data"]["url"]

            print "\n\n ###### debug 1 ######"
            print "subreddit " + subreddit
            #print "score " + str(score)
            print "over 18 " + str(over_18)
            print "post hint " + post_hint
            print "url " + url
            print "title " + title

            #check if banned
            if "subreddit" in bannedSubs:
                print "subreddit <" + str(subreddit) + "> is disallowed going for next one"
                pass
            else:

                #check if nsfw
                if over_18 == True:
                    print "subreddit <" + str(subreddit) + "> is NSFW going for next one"
                    pass
                else:
                    #check if image
                    if post_hint == "image":
                        print "we have image from subreddit " + str(subreddit)
                        
                        #determine type of image  
                        filenameTodiscard, contentTypeXCV = os.path.splitext(basename(url))                      
                        if contentTypeXCV == ".jpg":
                            file2dl = basename(url)

                            DLfile = urllib.URLopener()
                            DLfile.retrieve(url, file2dl)    
                            redFilename, contentType = os.path.splitext(file2dl)

                            #convert to 1:1
                            newimg = "23x-" + str(file2dl)
                            print "\nfile 2 dl is "+ file2dl + ". New image name is " + newimg
                            #subprocess.Popen(["sudo ./aspectcrop.sh -a 1:1 "+file2dl+newimg])
                            subprocess.call(['sudo', './aspectcrop.sh', '-a', '1:1', file2dl, newimg])
                        
                            newimg2 = "34x" + str(newimg)
                            print "\nnewimg is "+ newimg + ". New image 2 name is " + newimg2
                            #resize to 1080x1080 convert dragon.gif    -resize 64x64  resize_dragon.gif
                            subprocess.call(['sudo', 'convert', newimg, '-resize', '1080x1080', newimg2])

                            redFilename = newimg2
                            #todo: appdend it to list and delete file on next pass
                            print "we have pic "+ redFilename + " ready to go"

                            redTitle = title
                            redSub = subreddit


                            print "\n\n ###### debug 2 ######"
                            print "filename " + redFilename
                            print "redsub " + redSub
                            print "title " + redTitle
                            break


                        elif contentType == ".mp4":
                            #check if >3s <60s
                            print "we have video ready to go"
                            pass

                        else:
                            print "we got unusable " + str(contentType)
                            #if contentType in supportedPictureFormats:
                            #    print "converting pic to jpg"
                            #elif contentType in supportedMovingPictureFormats:
                            #    print "converting to mp4"
                            #else:
                            #    print "something is messy in get content function"                    

                    #or a link
                    elif post_hint == "link":
                        print "we have link from subreddit <" + str(subreddit) # + ">, lets bail"
                        pass

                    # if nothing lets pass
                    else:
                        print "no usable content in <" + str(subreddit) + ">"

        except Exception as e: 
            print(e)
            print "error on a" + str(a)
            return('error')
            pass
        
        a = a + 1
    #end of magic loop

    #print "done!"
    try:
            os.remove('testData.json')
    except OSError, e:  ## if failed, report it back to the user ##
            print ("Error: %s - %s." % (e.filename,e.strerror))

    print "\n##########"
    print "Title: " + redTitle + "\nSubreddit: " + redSub + "\nFilename: " + redFilename + "\nExtension: " + contentType
    print "##########\n"
    return(redTitle, redFilename, redSub, contentType)


def postPhoto(caption,photo):
    
    fileHeader = "<?php \n" \
    "set_time_limit(0);date_default_timezone_set('UTC');" \
    "require __DIR__.'/../vendor/autoload.php';" \
    "$username = '{}';$password = '{}';" \
    "$debug = true;$truncatedDebug = false;".format(user, pwd)

    photoHeader= "$photoFilename = '{}';$captionText = '{}';".format(photo, caption)

    endPhotoScript = '''
    $ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);try {
    $ig->setUser($username, $password);$ig->login();} catch (\Exception $e) {
    echo 'Something went wrong: '.$e->getMessage()."\n";exit(0);}try {
    $ig->timeline->uploadPhoto($photoFilename, ['caption' => $captionText]);
    } catch (\Exception $e) {echo 'Something went wrong: '.$e->getMessage()."\n";}
    '''

    filename = "postPhotoToIG" + ".php"
    script = open(filename, 'w')
    script.truncate()
    script.write(fileHeader)
    script.write("\n")
    script.write(photoHeader)
    script.write("\n")
    script.write(endPhotoScript)
    script.close()

    #run php script
    subprocess.Popen(["php", filename])
    
    #move this to be done with next iteration
    #try:
    #        os.remove(photo)
    #except OSError, e:  ## if failed, report it back to the user ##
    #        print ("Error: %s - %s." % (e.filename,e.strerror))

def main():
    print('Tick! The time is: %s' % datetime.now())
    try:
        if getContent() == 'error':
            print "something unexpected happened, trying again in an hour"
        else:
            # lets get content into variables
            caption, filename1, sub, extension = getContent()

            #make hashtags
            #to do: make this tuple
            if sub == "funny":
                hashtag = "#lol"

            elif sub == "aww":
                hashtag = "#aww"
    
            else:
                print "no hashtag"
                hashtag = ""
    
            caption = caption + " " + hashtag
            caption = caption.replace("'", "")
            caption = caption.replace('"', '')
            print "\n\nCAPTION IS\n\n" + str(caption)
            #print "\n\nwe are in the main, we recived: "+ sub, caption, filename1, extension
            #post to instagram
            if extension ==  ".jpg":
                #just to make it clear :p
                photo = filename1
                #print "in the if extension jpg, we have " + extension + " " + caption + " " + photo
                postPhoto(caption,photo)

            elif extension == ".mp4":
                video = filename1 + ".mp4"
                postVideo(caption,video)
        
            else:
                print "error in main()"
                print "recived extension is " + str(extension)
    except Exception as e: 
        print(e)
        print "error on the end of main bot function"